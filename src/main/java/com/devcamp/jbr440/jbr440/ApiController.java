package com.devcamp.jbr440.jbr440;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {
    @CrossOrigin
    @GetMapping("/invoices")
    public ArrayList<Invoice> getListInvoice() {
        ArrayList<Invoice> listInvoice = new ArrayList<Invoice>();

        Customer customer1 = new Customer(1, "QuanNM", 10);
        Customer customer2 = new Customer(2, "BoiHB", 15);
        Customer customer3 = new Customer(3, "ThuNHM", 20);

        Invoice invoice1 = new Invoice(1, customer1, 100);
        Invoice invoice2 = new Invoice(2, customer2, 200);
        Invoice invoice3 = new Invoice(3, customer3, 300);

        listInvoice.add(invoice1);
        listInvoice.add(invoice2);
        listInvoice.add(invoice3);

        return listInvoice;
    }

    // public static void main(String[] args) {
    //     Customer customer1 = new Customer(1, "QuanNM", 10);
    //     Customer customer2 = new Customer(2, "BoiHB", 15);
    //     Customer customer3 = new Customer(3, "ThuNHM", 20);
    //     System.out.println(customer1 + "," + customer2 + "," + customer3);
    //     System.out.println("----------------------------");
    //     Invoice invoice1 = new Invoice(1, customer1, 100);
    //     Invoice invoice2 = new Invoice(2, customer2, 200);
    //     Invoice invoice3 = new Invoice(3, customer3, 300);
    //     System.out.println(invoice1 + "," + invoice2 + "," + invoice3);
    //     System.out.println(invoice1.getAmountAfterDiscount());
    // }
}
