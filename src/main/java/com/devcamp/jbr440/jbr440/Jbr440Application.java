package com.devcamp.jbr440.jbr440;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr440Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr440Application.class, args);
	}

}
